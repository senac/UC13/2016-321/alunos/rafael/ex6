package br.com.senac.ex6;

import br.com.megaxhost.ex6.Valores;
import br.com.megaxhost.ex6.CalcularDesconto;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestCalcularDesconto {

    @Test
    public void testDesconto() {
       
        Valores vl = new Valores(100, 5, 1000);
        double rest = CalcularDesconto.getPreco(vl);
        assertEquals(490, rest, 0.1);
    }
}
