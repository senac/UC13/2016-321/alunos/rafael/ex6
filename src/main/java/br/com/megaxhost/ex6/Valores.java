package br.com.megaxhost.ex6;

public class Valores {

    private double precoIten;
    private double quantidade;
    private double fatorDesconto;
    private double limite;

    public Valores(double precoIten, double quantidade, double limite) {
        this.precoIten = precoIten;
        this.quantidade = quantidade;

        this.limite = limite;
    }

    public double getPrecoIten() {
        return precoIten;
    }

    public void setPrecoIten(double precoIten) {
        this.precoIten = precoIten;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public double getFatorDesconto() {
        return fatorDesconto;
    }

    public void setFatorDesconto(double fatorDesconto) {
        this.fatorDesconto = fatorDesconto;
    }

    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }

}
