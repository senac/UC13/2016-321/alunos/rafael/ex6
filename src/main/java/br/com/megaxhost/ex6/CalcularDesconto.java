package br.com.megaxhost.ex6;

public class CalcularDesconto {

    public static double getPreco(Valores v) {

        double precoBase = v.getQuantidade() * v.getPrecoIten();
        if (precoBase > v.getLimite()) {
            v.setFatorDesconto(0.95);

            return precoBase * v.getFatorDesconto();
        } else {
            v.setFatorDesconto(0.98);
            return precoBase * v.getFatorDesconto();
        }

    }

}
